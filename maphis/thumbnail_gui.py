import importlib.resources
import typing

import PySide6
from PySide6.QtCore import Signal, QSize
from PySide6.QtGui import QIcon, Qt, QPixmap, QImage
from PySide6.QtWidgets import QWidget

from maphis.common.photo import Photo
from maphis.ui_thumbnail_gui import Ui_ThumbnailGUI


class ThumbGUI(QWidget):
    resize_icon: typing.Optional[QIcon] = None
    ccw_icon: typing.Optional[QIcon] = None
    cw_icon: typing.Optional[QIcon] = None
    resolution_icon: typing.Optional[QIcon] = None
    delete_icon: typing.Optional[QIcon] = None

    resize_requested = Signal(Photo)
    rotate_requested = Signal(Photo, bool)
    resolution_setting_requested = Signal(Photo)
    save_photo = Signal(Photo)
    delete_photo_requested = Signal(Photo)

    def __init__(self, photo: Photo, parent: typing.Optional[PySide6.QtWidgets.QWidget] = None,
                 f: PySide6.QtCore.Qt.WindowFlags = Qt.WindowFlags()):
        super().__init__(parent, f)
        self.ui = Ui_ThumbnailGUI()
        self.ui.setupUi(self)

        self.setMaximumWidth(248)
        self.setMinimumWidth(248)
        self.setMinimumHeight(128)
        self.setMaximumHeight(128)

        self.photo = photo

        if self.resize_icon is None:
            # Placeholder icons, replace
            self.load_icons()

        self.ui.tbtnResize.setIcon(self.resize_icon)
        self.ui.tbtnRotateCW.setIcon(self.cw_icon)
        self.ui.tbtnRotateCCW.setIcon(self.ccw_icon)
        self.ui.tbtnSetResolution.setIcon(self.resolution_icon)
        self.ui.tbtnSave.setIcon(self.save_icon)
        self.ui.tbtnDelete.setIcon(self.delete_icon)

        self.ui.tbtnResize.clicked.connect(handler(self.resize_requested, photo))
        self.ui.tbtnRotateCW.clicked.connect(handler(self.rotate_requested, photo, True))
        self.ui.tbtnRotateCCW.clicked.connect(handler(self.rotate_requested, photo, False))
        self.ui.tbtnSetResolution.clicked.connect(handler(self.resolution_setting_requested, photo))
        self.ui.tbtnSave.clicked.connect(self._save)
        self.ui.tbtnSave.setVisible(self.photo.has_unsaved_changes)
        self.ui.tbtnDelete.clicked.connect(handler(self.delete_photo_requested, photo))

        self.ui.lblImgSize.setText(f'{photo.image_size[0]} \u00d7 {photo.image_size[1]}')
        if (scale := photo.image_scale) is not None and scale.magnitude > 0:
            self.ui.lblResolution.setText(f'{scale:.3f~P}')
        else:
            self.ui.lblResolution.setText('1 mm = ? px')

        approval = photo.approved['Labels']
        if approval is None:
            self.ui.lblApprovalInfo.setToolTip('Nothing approved yet')
        else:
            self.ui.lblApprovalInfo.setToolTip(f'Approved up to {approval}')

        self.setMouseTracking(True)

    def load_icons(self):
        icon_w = 24
        with importlib.resources.path('maphis.resources', 'resize.png') as path:
            im = QImage(str(path)).scaledToWidth(icon_w, Qt.TransformationMode.SmoothTransformation)
            self.resize_icon = QIcon(QPixmap.fromImage(im))
        with importlib.resources.path('maphis.resources', 'rotate.png') as path:
            im = QImage(str(path)).scaledToWidth(icon_w, Qt.TransformationMode.SmoothTransformation)
            self.cw_icon = QIcon(QPixmap.fromImage(im))
        with importlib.resources.path('maphis.resources', 'rotate.png') as path:
            im = QImage(str(path)).scaledToWidth(icon_w, Qt.TransformationMode.SmoothTransformation)
            img = QImage(im)
            self.ccw_icon = QIcon(QPixmap.fromImage(img.mirrored(True, False)))
        with importlib.resources.path('maphis.resources', 'ruler.png') as path:
            im = QImage(str(path)).scaledToWidth(icon_w, Qt.TransformationMode.SmoothTransformation)
            self.resolution_icon = QIcon(QPixmap.fromImage(im))
        with importlib.resources.path('maphis.resources', 'floppy-disk.png') as path:
            im = QImage(str(path)).scaledToWidth(icon_w, Qt.TransformationMode.SmoothTransformation)
            self.save_icon = QIcon(QPixmap.fromImage(im))
        with importlib.resources.path('maphis.resources', 'delete.png') as path:
            im = QImage(str(path)).scaledToWidth(icon_w, Qt.TransformationMode.SmoothTransformation)
            self.delete_icon = QIcon(QPixmap.fromImage(im))
        self.ui.tbtnResize.setIcon(self.resize_icon)
        self.ui.tbtnRotateCW.setIcon(self.cw_icon)
        self.ui.tbtnRotateCCW.setIcon(self.ccw_icon)
        self.ui.tbtnSetResolution.setIcon(self.resolution_icon)
        self.ui.tbtnSave.setIcon(self.save_icon)
        self.ui.tbtnDelete.setIcon(self.delete_icon)

        self.ui.tbtnResize.setFixedSize(icon_w, icon_w)
        self.ui.tbtnRotateCW.setFixedSize(icon_w, icon_w)
        self.ui.tbtnRotateCCW.setFixedSize(icon_w, icon_w)
        self.ui.tbtnSetResolution.setFixedSize(icon_w, icon_w)
        self.ui.tbtnSave.setFixedSize(icon_w, icon_w)
        self.ui.tbtnDelete.setFixedSize(icon_w, icon_w)

    def _save(self):
        self.save_photo.emit(self.photo)
        self.ui.tbtnSave.setVisible(False)

    def setMaximumWidth(self, maxw):
        super().setMaximumWidth(maxw)
        # self.load_icons()


def handler(signal: Signal, *args):
    def emit():
        signal.emit(*args)
    return emit