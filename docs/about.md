# About

## License

MAPHIS is open-source, released under [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.html).

## Authors

Developed at [CBIA](https://cbia.fi.muni.cz) by

Petr Matula, [pem@fi.muni.cz](pem@fi.muni.cz)<br/>
Radoslav Mráz, [radoslav.mraz95@gmail.com](radoslav.mraz95@gmail.com)<br/>
Matěj Pekár, [matej.pekar120@gmail.com](matej.pekar120@gmail.com)<br/>
Stano Pekár, [56765@muni.cz](56765@muni.cz)<br/>
Karel Štěpka, [172454@mail.muni.cz](172454@mail.muni.cz)

## How to Cite

Mráz, R., Štěpka, K., Pekár, M., Matula, P., & Pekár, S. (2023). MAPHIS—Measuring arthropod phenotypes using hierarchical image segmentations. *Methods in Ecology and Evolution*, https://doi.org/10.1111/2041-210X.14250

### Bibtex

```bibtex
@article{https://doi.org/10.1111/2041-210X.14250,
    author = {Mráz, Radoslav and Štěpka, Karel and Pekár, Matěj and Matula, Petr and Pekár, Stano},
    title = {MAPHIS—Measuring arthropod phenotypes using hierarchical image segmentations},
    journal = {Methods in Ecology and Evolution},
    volume = {n/a},
    number = {n/a},
    pages = {},
    keywords = {arachnids, arthropods, convolutional neural networks, hierarchical segmentation, image analysis, insects, machine vision, morphological traits},
    doi = {https://doi.org/10.1111/2041-210X.14250},
    url = {https://besjournals.onlinelibrary.wiley.com/doi/abs/10.1111/2041-210X.14250},
    eprint = {https://besjournals.onlinelibrary.wiley.com/doi/pdf/10.1111/2041-210X.14250},
    year = {2023}
}
```
