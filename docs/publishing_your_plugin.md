# Publishing your plugin
**NOTE** This tutorial is under construction. Moreover, the system for publishing plugins is still being devised.

For now, `MAPHIS` does not provide an official way of publishing plugins, so at the moment you have to share your plugin folder with your target audience on your own. However, feel free to reach out to us for a discussion on including your plugin in the default set of plugins offered by `MAPHIS` (you can find our emails in the `Help->About MAPHIS` dialog in `MAPHIS`).